// import { useState, useEffect } from 'react';
// import React from "react";
// import Card from "./components/Card/Card";
// import Header from "./components/Header/Header";
// import CartDrawer from "./components/CartDrawer/CartDrawer";
// import Modal from "./components/Modal/Modal";

// const card = [
//   {
//     name: "Nike Air Max 270",
//     price: "140",
//     path: "/img/air-max-270.jpeg",
//     article: "1",
//     color: "Green",
//   },
//   {
//     name: "Nike Air Max 260",
//     price: "130",
//     path: "./img/air-max-270-whi.jpeg",
//     article: "2",
//     color: "White",
//   },
//   {
//     name: "Nike Air Max 230",
//     price: "100",
//     path: "./img/air-max-230.jpeg",
//     article: "3",
//     color: "White",
//   },
//   {
//     name: "Nike Spark",
//     price: "130",
//     path: "./img/nike-spark.jpg",
//     article: "4",
//     color: "Beige",
//   },
//   {
//     name: "Nike Air Max 90",
//     price: "130",
//     path: "./img/nike-air-max-90.jpg",
//     article: "5",
//     color: "Pink",
//   },
//   {
//     name: "Nike Air Huarache",
//     price: "150",
//     path: "./img/nike-air-huarache.jpg",
//     article: "6",
//     color: "Lilac",
//   },
//   {
//     name: "Nike Air Max Dawn",
//     price: "120",
//     path: "./img/air-max-dawn.jpeg",
//     article: "7",
//     color: "White",
//   },
//   {
//     name: "Nike Giannis Immortality",
//     price: "120",
//     path: "./img/immortality.jpg",
//     article: "8",
//     color: "Blue",
//   },
//   {
//     name: "Nike Air Max",
//     price: "100",
//     path: "./img/max.jpeg",
//     article: "9",
//     color: "Green",
//   },
//   {
//     name: "Nike Trainers",
//     price: "120",
//     path: "./img/trainers.jpeg",
//     article: "10",
//     color: "Black",
//   },
//   {
//     name: "Nike Air VaporMax",
//     price: "90",
//     path: "./img/vaporMax.jpg",
//     article: "11",
//     color: "Light-Green",
//   },
//   {
//     name: "Nike Trainers",
//     price: "100",
//     path: "./img/nike-ca.jpg",
//     article: "12",
//     color: "Yellow",
//   },
// ];

// function App() {
//   const [cart, setCart] = useState(
//     localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : []
//   );

//   useEffect(() => {
//     const storedCart = JSON.parse(localStorage.getItem('cart'));
//     if (storedCart) {
//       setCart(storedCart);
//     }
//   }, []);

//   const [cartOpened, setCartOpened] = React.useState(false); // Состояние для показа/скрытия корзины

//   const [isModalOpen, setIsModalOpen] = useState(false); // Состояние для показа/скрытия модального окна

//   const [selectedProduct, setSelectedProduct] = useState(null); // Выбранный продукт для добавления в корзину

//   const [cartItems, setCartItems] = useState([]); // Элементы в корзине

//   const [stateCart, setStateCart] = useState([]);
//   console.log(stateCart);

//   const appStateCart = (article) => {
//     setStateCart((prevState) => {
//       if (!prevState.includes(article)) {
//         const newState = [...prevState, article];
//         localStorage.setItem("cart", JSON.stringify(newState));
//         return newState;
//       } else {
//         return prevState;
//         // return prevState.filter((item) => item!== article);
//       }
//     });
//   };

//   const handleCartClick = (product) => { // Обработчик клика на кнопке добавления продукта в корзину
//     setSelectedProduct(product);
//     setIsModalOpen(true);
//   };

//   const handleModalClose = () => { // Обработчик закрытия модального окна
//     setSelectedProduct(null);
//     setIsModalOpen(false);
//   };

//   const handleAddToCart = (product) => { // Обработчик добавления продукта в корзину
//     const updatedCart = [...cart, product];
//     setCart(updatedCart);
//     localStorage.setItem('cart', JSON.stringify(updatedCart));
//   };

//   return (
//     <div className="wrapper clear">
//       {/* можно написать так {cartOpened ?
// <CartDrawer onClose={() => setCartOpened(false)} /> : null}
// или переписать в такой формат {cartOpened && <CartDrawer onClose={() => setCartOpened(false)} />} работать будут одинаково, но во 2-м значение react будет говорить если 1-е значение true то выполни 2-ю часть*/}
//       {cartOpened ? <CartDrawer onClose={() => setCartOpened(false)} /> : null}
//       <Header onClickCart={() => setCartOpened(true)} />
//       <div className="content pl-40">
//         <div className="d-flex align-center justify-between">
//           <h2>All sneakers</h2>
//           <div className="search-block d-flex ">
//             <img src="/svg/search.svg" alt="Search" />
//             <input placeholder="Search"></input>
//           </div>
//         </div>
//         <div className="cardSection">
//           {card.map((product) => (
//             <Card
//             key={product.article}
//             name={product.name}
//             price={product.price}
//             path={product.path}
//             article={product.article}
//             color={product.color}
//             onClickFavoriteBtn={() => console.log('added to favorites')}
//             onClickPlusBtn={() => handleCartClick(product)}
//             isInCart={cart.includes(product)}
//             />
//           ))}
//         </div>
//       </div>
//       {selectedProduct && (
//         <Modal onClose={handleModalClose}>
//           <Card
//             key={selectedProduct.article}
//             name={selectedProduct.name}
//             price={selectedProduct.price}
//             path={selectedProduct.path}
//             article={selectedProduct.article}
//             color={selectedProduct.color}
//             onClickFavoriteBtn={() => console.log('added to favorites')}
//             onClickPlusBtn={() => {
//               handleAddToCart(selectedProduct);
//               handleModalClose();
//             }}
//             isInCart={cart.includes(selectedProduct)}
//           />
//         </Modal>
//       )}
//     </div>
//   );
// }

// export default App;

// import { Component } from "react";
// import "./App.css";
// import Button from "./components/Button/Button";
// import Header from "./components/Header/Header";
// import Modal from "./components/Modal/Modal";
// import CardContainer from "./components/CardContainer/CardContainer";

// class App extends Component {
//   state = {
//     firstModalDisplay: false,
//     favourites: [],
//     cards: [],
//     carts: [],
//     modalProps: {},
//   };

//   setModalProps = (value) => {
//     this.setState({ modalProps: value });
//   };

//   async componentDidMount() {
//     const cards = await fetch(`./items.json`).then((res) => res.json());
//     cards.forEach((card) => {
//       card.isFavourite =
//         localStorage.getItem("Favourites") &&
//         JSON.parse(localStorage.getItem("Favourites")).includes(card.article);
//     });
//     this.setState({ cards });
//     if (localStorage.getItem("carts")) {
//       const carts = await JSON.parse(localStorage.getItem("carts"));
//       this.setState({ carts });
//     }
//     if (localStorage.getItem("favourites")) {
//       const favourites = await JSON.parse(localStorage.getItem("favourites"));
//       this.setState({ favourites });
//     }
//   }

//   openFirstModal = () => {
//     this.setState({ firstModalDisplay: true });
//   };

//   closeFirstModal = () => {
//     this.setState({ firstModalDisplay: false });
//   };

//   addToCart = (article) => {
//     this.setState((current) => {
//       const carts = [...current.carts];
//       const index = carts.findIndex((el) => el.article === article);

//       if (index === -1) {
//         carts.push({ ...article, count: 1 });
//       } else {
//         carts[index].count += 1;
//       }

//       localStorage.setItem("carts", JSON.stringify(carts));
//       this.closeFirstModal();
//       return { carts };
//     });
//   };

//   addToFavourites = (article) => {
//     this.setState((current) => {
//       let favourites = [...current.favourites];
//       if (current.favourites.includes(article)) {
//         favourites = current.favourites.filter((el) => {
//           return el !== article;
//         });
//       } else {
//         favourites = [...current.favourites, article];
//       }
//       localStorage.setItem("favourites", JSON.stringify(favourites));
//       this.closeFirstModal();
//       return { favourites };
//     });
//   };

//   render() {
//     const { firstModalDisplay, cards, carts, modalProps, favourites } =
//       this.state;

//     return (
//       <>
//         <Header carts={carts} favourites={favourites} />
//         <main>
//           <section>
//             <h1>Каталог</h1>
//             <CardContainer
//               favourites={favourites}
//               addToCart={this.addToCart}
//               cards={cards}
//               carts={carts}
//               openFirstModal={this.openFirstModal}
//               setModalProps={this.setModalProps}
//               addToFavourites={this.addToFavourites}
//             />
//           </section>
//         </main>

//         {firstModalDisplay && (
//           <Modal
//             className="modal"
//             firstModalDisplay={firstModalDisplay}
//             closeButton={true}
//             close={this.closeFirstModal}
//             modalProps={modalProps}
//             addToCart={this.addToCart}
//             actions={[
//               <Button
//                 key={1}
//                 onClick={() => {
//                   this.addToCart(modalProps.article);
//                 }}
//                 className="modal-button"
//                 text="Add"
//               />,
//               <Button
//                 key={2}
//                 onClick={this.closeFirstModal}
//                 className="modal-button"
//                 text="Cancel"
//               />,
//             ]}
//           />
//         )}
//       </>
//     );
//   }
// }

// export default App;

import React, { useState, useEffect } from "react";
import "./App.css";
import Button from "./components/Button/Button";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";
import CardContainer from "./components/CardContainer/CardContainer";

function App() {
  const [firstModalDisplay, setFirstModalDisplay] = useState(false);
  const [favourites, setFavourites] = useState([]);
  const [cards, setCards] = useState([]);
  const [carts, setCarts] = useState([]);
  const [modalProps, setModalProps] = useState({});

  const openFirstModal = () => {
    setFirstModalDisplay(true);
  };

  const closeFirstModal = () => {
    setFirstModalDisplay(false);
  };

  const addToCart = (article) => {
    setCarts((current) => {
      const updatedCarts = [...current];
      const index = updatedCarts.findIndex((el) => el.article === article);

      if (index === -1) {
        updatedCarts.push({ ...article, count: 1 });
      } else {
        updatedCarts[index].count += 1;
      }

      localStorage.setItem("carts", JSON.stringify(updatedCarts));
      closeFirstModal();
      return updatedCarts;
    });
  };

  const addToFavourites = (article) => {
    setFavourites((current) => {
      let updatedFavourites = [...current];
      if (current.includes(article)) {
        updatedFavourites = current.filter((el) => {
          return el !== article;
        });
      } else {
        updatedFavourites = [...current, article];
      }
      localStorage.setItem("favourites", JSON.stringify(updatedFavourites));
      closeFirstModal();
      return updatedFavourites;
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      const fetchedCards = await fetch(`./items.json`).then((res) =>
        res.json()
      );
      const updatedCards = fetchedCards.map((card) => {
        return {
          ...card,
          isFavourite:
            localStorage.getItem("favourites") &&
            JSON.parse(localStorage.getItem("favourites")).includes(
              card.article
            ),
        };
      });
      setCards(updatedCards);
    };
    fetchData();

    if (localStorage.getItem("carts")) {
      const updatedCarts = JSON.parse(localStorage.getItem("carts"));
      setCarts(updatedCarts);
    }
    if (localStorage.getItem("favourites")) {
      const updatedFavourites = JSON.parse(localStorage.getItem("favourites"));
      setFavourites(updatedFavourites);
    }
  }, []);

  return (
    <>
      <Header carts={carts} favourites={favourites} />
      <main className="wrapper clear">
        <section className="content pl-40">
        <div className="d-flex align-center justify-between">
           <h2>All sneakers</h2>
           <div className="search-block d-flex ">
             <img src="/svg/search.svg" alt="Search" />
            <input placeholder="Search"></input>
          </div>
        </div>
          <CardContainer
            favourites={favourites}
            addToCart={addToCart}
            cards={cards}
            carts={carts}
            openFirstModal={openFirstModal}
            setModalProps={setModalProps}
            addToFavourites={addToFavourites}
          />
        </section>
      </main>

      {firstModalDisplay && (
        <Modal
          className="modal"
          firstModalDisplay={firstModalDisplay}
          closeButton={true}
          close={closeFirstModal}
          modalProps={modalProps}
          addToCart={addToCart}
          actions={[
            <Button
              key="addToCart"
              onClick={() => {
                addToCart(modalProps);
              }}
              className="modal-button"
                text="Add"
            ></Button>,
            <Button
              key="addToFavourites"
              onClick={() => {
                addToFavourites(modalProps);
              }}
              className="modal-button"
                text="Cancel"
            >
              {favourites.includes(modalProps)
                ? "Deleted from favorites"
                : "Added to favorites"}
            </Button>,
          ]}
        >
          <h2>{modalProps.name}</h2>
          <img src={modalProps.img} alt={modalProps.name} />
          <p>{modalProps.description}</p>
        </Modal>
      )}
    </>
  );
}

export default App;
