import { PureComponent } from "react";
import styles from "./Header.module.scss";
import cartIcon from "../../components/icons/cart-outline.svg";
import favIcon from "../../components/icons/star_like.svg";

class Header extends PureComponent {
  render() {
    let cartQt;
    let favouriteQt;
    if (localStorage.getItem("carts")) {
      cartQt = <span>{JSON.parse(localStorage.getItem("carts")).length}</span>;
    } else {
      cartQt = <span>0</span>;
    }

    if (localStorage.getItem("favourites")) {
      favouriteQt = (
        <span>{JSON.parse(localStorage.getItem("favourites")).length}</span>
      );
    } else {
      favouriteQt = <span>0</span>;
    }

    return (
      <header className={styles.root}>
         <div className="d-flex align-center">
        <img width={40} src="/img/logo.jpg" />
        <div>
          <h3>REACT NIKE SHOP</h3>
           <p className="opacity-6">The best shop of sneakers!</p>
        </div>
      </div>
        <ul>
          <li className="cu-p">
            <a href="#">
              <img src={cartIcon} alt="Cart" />
            </a>
          </li>
          <li>{cartQt}</li>
        </ul>
        <ul>
          <li className="cu-p">
            <a href="#">
              <img src={favIcon} alt="Cart" width={22} />
            </a>
          </li>
          <li>{favouriteQt}</li>
        </ul>
      </header>
    );
  }
}

export default Header;

