import React, { PureComponent } from "react";
import styles from "./Modal.module.scss";

class Modal extends PureComponent {
  render() {
    const { header, closeButton, text, actions, handleOnClose } = this.props;
    return (
      <>
        <div className={styles.modalBackground} onClick={handleOnClose}></div>
        <div className={styles.modal}>
          <header className={styles.modalHeader}>
            <span>{header}</span>
            {closeButton && (
              <button
                type="button"
                className={styles.modalCloseBtn}
                onClick={handleOnClose}
              >
                X
              </button>
            )}
          </header>
          <div className={styles.modalBody}>
            <p>{text}</p>
          </div>
          <footer className={styles.modalFooter}>{actions}</footer>
        </div>
      </>
    );
  }
}

export default Modal;
