import React, { Component } from "react";
import styles from "./App.module.scss";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
  state = {
    openModal: false,
  };

  openModal = (flag) => this.setState({ openModal: flag });
  closeModal = () => this.setState({ openModal: false });

  render() {
    const { openModal } = this.state;
    return (
      <div className={styles.wrapper}>
        <div className={styles.buttonContainer}>
          <Button
            onClick={() => this.openModal("firstModal")}
            text="Open first modal"
            backgroundColor="rgb(87 91 228)"
          />
          <Button
            onClick={() => this.openModal("secondModal")}
            text="Open second modal"
            backgroundColor="rgb(190, 90, 8)"
          />
        </div>
        {openModal === "firstModal" && (
          <Modal
            header="Do you want to delete this file?"
            text="Once you delete this file, it won’t be possible to undo this action.
				Are you sure you want to delete it?"
            handleOnClose={this.closeModal}
            closeButton
            actions={
              <div>
                <Button
                  onClick={this.closeModal}
                  text="Ok"
                  backgroundColor="rgb(0, 0, 0, 0.4)"
                />
                <Button
                  onClick={this.closeModal}
                  text="Cancel"
                  backgroundColor="rgb(0, 0, 0, 0.4)"
                />
              </div>
            }
          />
        )}
        {openModal === "secondModal" && (
          <Modal
            header="Second Modal"
            text="Say, Hello!"
            handleOnClose={this.closeModal}
            actions={
              <div>
                <Button
                  onClick={() => {
                    console.log("HELLO!");
                  }}
                  text="ConsoleLog"
                  backgroundColor="rgb(13, 106, 121, 0.5)"
                />
                <Button
                  onClick={() => alert("HELLO!")}
                  text="Alert"
                  backgroundColor="rgb(13, 106, 121, 0.5)"
                />
              </div>
            }
          />
        )}
      </div>
    );
  }
}

export default App;