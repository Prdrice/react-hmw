import React from "react";
import CardContainer from "../components/CardContainer/CardContainer";

const MainPage = () => {
  return (
    <main>
      <section>
        <h2>All sneakers</h2>
        <CardContainer/>
      </section>
    </main>
  );
};

export default MainPage;
