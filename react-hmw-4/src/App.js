import React from "react";
import { useEffect } from "react";
import "./App.css";
import Button from "./components/Button/Button";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";
import { BrowserRouter as Router } from "react-router-dom";
import AppRoutes from "./AppRoutes.js";
import { loadItems } from "./redux/middleware";
import { useDispatch, useSelector } from "react-redux";
import { ADD_TO_TRASH, REMOVE_FROM_TRASH, CLOSE_MODAL } from "./redux/types";

function App() {
  const isLoading = useSelector((state) => state.items.isLoading);
  const dispatch = useDispatch();
  const isOpen = useSelector((state) => state.modal.isOpen);
  const thisItem = useSelector((state) => state.modal.data);
  const infoModal = useSelector((state) => state.modal.info);

  const closeModal = () => {
    dispatch({ type: CLOSE_MODAL });
  };

  const addToTrash = (data) => {
    dispatch({ type: ADD_TO_TRASH, payload: data });
  };

  const removeFromTrash = (data) => {
    dispatch({ type: REMOVE_FROM_TRASH, payload: data });
  };

  useEffect(() => {
    dispatch(loadItems());
  }, [dispatch]);

  if (isLoading) {
    return <div className="App">Loading... Please wait a bit!</div>;
  }

  return (
    <Router>
      <>
        <Header />
        <main className="wrapper clear">
          <section className="content pl-40">
            {/* <div className="d-flex align-center justify-between">
              <h2>All sneakers</h2>
              <div className="search-block d-flex ">
                <img src="/svg/search.svg" alt="Search" />
                <input placeholder="Search"></input>
              </div>
            </div> */}
            <AppRoutes />
          </section>
        </main>

        {isOpen && (
          <Modal
            className="modal"
            closeButton={true}
            closeModal={closeModal}
            actions={[
              <Button
                key={1}
                onClick={() => {
                  if (infoModal === "Add to trash") {
                    addToTrash(thisItem.article);
                  }
                  if (infoModal === "Remove from trash") {
                    removeFromTrash(thisItem.article);
                  }
                  closeModal();
                }}
                className="modal-button"
                text="Confirm"
              />,
              <Button
                key={2}
                onClick={closeModal}
                className="modal-button"
                text="Cancel"
              />,
            ]}
          />
        )}
      </>
    </Router>
  );
}

export default App;
