import React from "react";
import CardItem from "../CardItem";
import styles from "./CardContainer.module.scss";
import { useSelector, shallowEqual } from "react-redux";

const CardContainer = () => {
  const cards = useSelector((state) => state.items.data, shallowEqual);

  return (
    <div>
      <ul className={styles.list}>
        {cards.map((item) => (
          <li key={item.path}>
            <CardItem
              key={item.article}
              item={item}
              toCart
            />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default CardContainer;
