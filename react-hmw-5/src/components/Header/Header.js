import styles from "./Header.module.scss";
import cartIcon from "../../components/icons/cart-outline.svg";
import favIcon from "../../components/icons/star_like.svg";
import { NavLink } from "react-router-dom";
import { useSelector, shallowEqual } from "react-redux";

const Header = ({ cartQt, favouriteQt }) => {
  const items = useSelector((state) => state.items.data, shallowEqual);
  const favourites = items.filter((item) => item.favourite);
  const trashItems = items.filter((item) => item.inTrashAmount);

  if (trashItems.length) {
    cartQt = <span>{trashItems.length}</span>;
  } else {
    cartQt = <span>0</span>;
  }

  if (favourites.length) {
    favouriteQt = <span>{favourites.length}</span>;
  } else {
    favouriteQt = <span>0</span>;
  }

  return (
    <header className={styles.root}>
      {/* <div className="d-flex align-center">
        <img width={40} src="/img/logo.jpg" />
        <div>
          <h3>REACT NIKE SHOP</h3>
           <p className="opacity-6">The best shop of sneakers!</p>
        </div>
      </div> */}
      <ul>
        <li>
          <NavLink to="/">Main</NavLink>
        </li>
        <li>
          <NavLink to="/cart">
            Cart
            <img src={cartIcon} alt="Cart" />
          </NavLink>
          {cartQt}
        </li>
        <li>
          <NavLink to="/favourites">
            Favorites
            <img src={favIcon} alt="Cart" width={26} />
            {favouriteQt}
          </NavLink>
        </li>
      </ul>
    </header>
  );
};

export default Header;
