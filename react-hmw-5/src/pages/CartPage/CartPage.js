import React from "react";
import CardItem from "../../components/CardItem";
import styles from "./CartPage.module.scss";
import { getTrash } from "../../redux/middleware";
import { useSelector, shallowEqual } from "react-redux";
import FormPage from "../../components/Form/FormPage";


const CartPage = () => {
  const items = useSelector((state) => state.items.data, shallowEqual);
  const carts = items.filter((item) => item.inTrashAmount);


  // if (!getTrash()) {
  //   return <h2 className="container">Empty</h2>;
  // }

  if (JSON.parse(localStorage.getItem("trash"))) {
    return <h2 className="container">Empty</h2>;
  }

  return (
    <>
      <h2>Registration Form</h2>
      <FormPage />
      <h2>Cart</h2>
      <div className={styles.cart}>
      {carts.map((item) => {
          return <CardItem key={item.article} item={item} fromCart />;
        })}
      </div>
    </>
  );
};

export default CartPage;


