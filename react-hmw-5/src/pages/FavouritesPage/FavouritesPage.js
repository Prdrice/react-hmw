import React from "react";
import CardItem from "../../components/CardItem";
import styles from "./FavouritesPage.module.scss";
import { useSelector, shallowEqual } from "react-redux";

const FavouritesPage = () => {
  const items = useSelector((state) => state.items.data, shallowEqual);
  const favourites = items.filter((item) => item.favourite);

  if (JSON.parse(localStorage.getItem("favourites"))) {
    return <h2 className="container">Empty</h2>;
  }

  return (
    <>
      <h2>Favourites</h2>
      <div className={styles.favourite}>
        {favourites.map((item) => {
          return (
            <CardItem
              favourites={favourites.includes(item.article)}
              key={item.path}
              item={item}
              // openFirstModal={openFirstModal}
              // setModalProps={setModalProps}
              // addToFavourites={addToFavourites}
              toCart
            />
          );
        })}
      </div>
    </>
  );
};

export default FavouritesPage;


