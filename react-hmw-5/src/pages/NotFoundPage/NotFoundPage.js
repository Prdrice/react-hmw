import styles from "./NotFoundPage.module.scss";
import Button from "../../components/Button/Button";
import { useNavigate, useLocation } from "react-router-dom";

const NotFoundPage = () => {
  const { pathname } = useLocation();
  const navigate = useNavigate();

  return (
    <div>
      <h2 className={styles.title}>
        {" "}
        This page <span className={styles.path}>{pathname}</span> is not found{" "}
      </h2>
      <Button
        onClick={() => {
          navigate("/");
        }}
        text="Return on Main"
        className="modal-button"
      >
        Back to Home
      </Button>
    </div>
  );
};

export default NotFoundPage;
