import { getTrash, setTrash, getFavorites, setFavorites } from "./middleware";
import produce from "immer";
import {
  SUCCESS,
  SHOW_MODAL,
  ADD_REMOVE_FAVORITE,
  CLOSE_MODAL,
  ADD_TO_TRASH,
  REMOVE_FROM_TRASH,
  MAKE_ORDER,
} from "./types";

const initialState = {
  items: {
    data: [],
    isLoading: true,
  },
  modal: {
    info: "",
    data: null,
    isOpen: false,
  },
  order: {},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS:
      return {
        //Возвращает новый объект состояния с помощью оператора spread
        ...state,
        items: { ...state.data, data: action.payload, isLoading: false },
      }; // Обновляет свойство "items"
    case SHOW_MODAL:
      return {
        ...state,
        modal: {
          ...state.modal, //Обновляет свойство "modal" новым объектом
          info: action.payload.info, //Обновляет свойства
          data: action.payload.data,
          isOpen: true, //Устанавливает свойство "isOpen" в значение "true", чтобы открыть модальное окно.
        },
      };
    case CLOSE_MODAL:
      return { ...state, modal: { ...state.modal, isOpen: false } };
    //Обновляет свойство "modal" новым объектом, который также использует оператор spread для копирования свойств объекта "state.modal".

    case ADD_REMOVE_FAVORITE:
      return produce(state, (draftState) => {
        //Возвращает измененное состояние (state) с помощью функции produce, которая обеспечивает иммутабельное обновление состояния.
        draftState.items.data.forEach((el) => {
          //Итерирует по каждому элементу объекта
          if (el.article === action.payload) {
            el.favourite = !el.favourite;
            //Если условие выполняется, то меняет значение свойства "favourite" текущего элемента на противоположное (инвертирует его)

            const favourites = getFavorites();
            //Получает список избранных элементов с помощью функции
            if (favourites.includes(el.article)) {
              favourites.splice(favourites.indexOf(el.article), 1);
              //Если список избранных элементов (favourites) содержит значение свойства "article" текущего элемента, то удаляет это значение из списка.
            } else {
              favourites.push(el.article);
              //Если не содержит, то добавляет это значение в список.
            }
            setFavorites(favourites);
            //Обновляет список избранных элементов, вызывая функцию
          }
          return el;
        });
      });

    case ADD_TO_TRASH:
      return produce(state, (draftState) => {
        draftState.items.data.forEach((el) => {
          if (el.article === action.payload) {
            //Проверяет, если значение свойства "article" текущего элемента равно значению action.payload
            el.inTrashAmount++;
            //Если условие выполняется, то увеличивается на 1
            const carts = getTrash();
            if (Object.keys(carts).includes(el.article)) {
              carts[el.article]++;
              //Если ключ "article" текущего элемента присутствует в объекте корзины (trash), то увеличивает значение этого ключа на 1.
            } else {
              carts[el.article] = 1;
              // carts.push(el.article);
              //Если ключ "article"отсутствует в объекте корзины, то устанавливает значение этого ключа равным 1.
            }
            setTrash(carts);
          }
          return el;
        });
      });

    case REMOVE_FROM_TRASH:
      return produce(state, (draftState) => {
        draftState.items.data.forEach((el) => {
          if (el.article === action.payload) {
            el.inTrashAmount--;
            ////Если условие выполняется, то уменьшается на 1
            const trash = getTrash();
            trash[el.article]--;
            //Уменьшает значение ключа "article" в объекте корзины (trash) на 1.
            if (trash[el.article] === 0) {
              delete trash[el.article];
              //Если значение ключа "article" в объекте корзины (trash) становится равным 0, то удаляет этот ключ из объекта корзины.
            }
            setTrash(trash);
          }
          return el;
        });
      });

    case MAKE_ORDER:
      return produce(state, (draftState) => {
        draftState.order = action.payload;
      });

    default:
      return state;
    //В случае, если тип действия не соответствует ни одному из указанных в case-выражениях (default case), он просто возвращает неизменное состояние (state).
  }
};

export default reducer;

