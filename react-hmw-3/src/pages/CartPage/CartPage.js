import React from "react";
import CardItem from "../../components/CardItem";
import styles from "./CartPage.module.scss";

const CartPage = ({
  favourites,
  addToCart,
  cards,
  carts,
  openFirstModal,
  setModalProps,
  addToFavourites,
}) => {
  if (!JSON.parse(localStorage.getItem("carts"))) {
    return <h2 className="container">Empty</h2>;
  }

  const cart = JSON.parse(localStorage.getItem("carts"));
  const keys = Object.values(cart);
  const render = cards.filter((item) => {
    return keys.includes(item.article.toString());
  });

  return (
    <>
      <h2>Cart</h2>
      <div className={styles.cart}>
        {render.map((item) => {
          return (
            <CardItem
              favourites={favourites.includes(item.article)}
              key={item.article}
              addToCart={addToCart}
              item={item}
              openFirstModal={openFirstModal}
              setModalProps={setModalProps}
              addToFavourites={addToFavourites}
              fromCart
            />
          );
        })}
      </div>
    </>
  );
};

export default CartPage;
