import React from "react";
import { useState, useEffect } from "react";
import "./App.css";
import Button from "./components/Button/Button";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";
import { BrowserRouter as Router } from "react-router-dom";
import AppRoutes from "./AppRoutes.js";

function App() {
  const [firstModalDisplay, setFirstModalDisplay] = useState(false);
  const [favourites, setFavourites] = useState([]);
  const [cards, setCards] = useState([]);
  const [carts, setCarts] = useState([]);
  const [modalProps, setModalProps] = useState({});
  const [modalAction, setModalAction] = useState("Add to cart");

  // Этот код определяет функцию openFirstModal, которая устанавливает состояние modalAction в "Add to cart" или "Remove from cart", в зависимости от переданного в функцию аргумента text. Затем устанавливается состояние firstModalDisplay в true, что приводит к отображению модального окна. Предполагается, что после вызова этой функции модальное окно будет показывать сообщение с действием для пользователя.

  const openFirstModal = (text) => {
    if (text === "Add to cart") {
      setModalAction("Add to cart");
    }
    if (text === "Remove from cart") {
      setModalAction("Remove from cart");
    }
    setFirstModalDisplay(true);
  };

  const closeFirstModal = () => {
    setFirstModalDisplay(false);
  };

  const addToCart = (article) => {
    setCarts((current) => {
      // Создаем копию текущего состояния корзины
      const updatedCarts = [...current];
      // Ищем индекс товара article в массиве updatedCarts
      const index = updatedCarts.findIndex((item) => item === article);

      if (index === -1) {
        // Если товар не найден в корзине, добавляем его
        updatedCarts.push(article);
      } else {
        // Если товар уже есть в корзине, увеличиваем его количество (count) на 1
        updatedCarts[index].count += 1;
      }

      // Объединяем дублирующиеся товары в корзине
      const mergedCarts = updatedCarts.reduce((acc, item) => {
        const existingItem = acc.find((i) => i === item);

        if (existingItem) {
          existingItem.count += 1;
        } else {
          acc.push(item);
        }

        return acc;
      }, []);

      // Сохраняем обновленную корзину в localStorage
      localStorage.setItem("carts", JSON.stringify(mergedCarts));

      // Закрываем модальное окно
      closeFirstModal();

      // Возвращаем объединенную корзину для обновления состояния компонента
      return mergedCarts;
    });
  };

  const deleteThisItem = (article) => {
    setCarts((current) => {
      const carts = [...current];
      const index = carts.findIndex((el) => el === article);

      if (index !== -1) {
        carts.splice(index, 1);
      }

      localStorage.setItem("carts", JSON.stringify(carts));
      closeFirstModal();
      return carts;
    });
  };

  const addToFavourites = (article) => {
    setFavourites((current) => {
      // Создаем копию текущего состояния избранных товаров
      const updatedFavourites = [...current];
      // Ищем индекс товара article в массиве updatedFavourites
      const index = updatedFavourites.findIndex((item) => item === article);

      if (index === -1) {
        // Если товар не найден в избранных, добавляем его
        updatedFavourites.push(article);
      } else {
        // Если товар уже есть в избранных, удаляем его из списка
        updatedFavourites.splice(index, 1);
      }

      // Сохраняем обновленные избранные товары в localStorage
      localStorage.setItem("favourites", JSON.stringify(updatedFavourites));

      // Возвращаем обновленный список избранных товаров для обновления состояния компонента
      return updatedFavourites;
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      const fetchedCards = await fetch(`./items.json`).then((res) =>
        res.json()
      );
      const updatedCards = fetchedCards.map((card) => {
        return {
          ...card,
          isFavourite:
            localStorage.getItem("favourites") &&
            JSON.parse(localStorage.getItem("favourites")).includes(
              card.article
            ),
        };
      });
      setCards(updatedCards);
    };
    fetchData();

    if (localStorage.getItem("carts")) {
      const updatedCarts = JSON.parse(localStorage.getItem("carts"));
      setCarts(updatedCarts);
    }
    if (localStorage.getItem("favourites")) {
      const updatedFavourites = JSON.parse(localStorage.getItem("favourites"));
      setFavourites(updatedFavourites);
    }
  }, []);

  return (
    <Router>
      <>
        <Header carts={carts} favourites={favourites} />
        <main className="wrapper clear">
          <section className="content pl-40">
            {/* <div className="d-flex align-center justify-between">
              <h2>All sneakers</h2>
              <div className="search-block d-flex ">
                <img src="/svg/search.svg" alt="Search" />
                <input placeholder="Search"></input>
              </div>
            </div> */}
            <AppRoutes
              deleteThisItem={deleteThisItem}
              favourites={favourites}
              addToCart={addToCart}
              cards={cards}
              carts={carts}
              openFirstModal={openFirstModal}
              setModalProps={setModalProps}
              addToFavourites={addToFavourites}
              modalAction={modalAction}
            />
            {/* <CardContainer
              favourites={favourites}
              addToCart={addToCart}
              cards={cards}
              carts={carts}
              openFirstModal={openFirstModal}
              setModalProps={setModalProps}
              addToFavourites={addToFavourites}
            /> */}
          </section>
        </main>

        {firstModalDisplay && (
          <Modal
            text={`${modalAction} '${modalProps.name}'?`}
            className="modal"
            firstModalDisplay={firstModalDisplay}
            closeButton={true}
            close={closeFirstModal}
            modalProps={modalProps}
            addToCart={addToCart}
            setModalProps={setModalProps}
            actions={[
              // <Button
              //   key="addToCart"
              //   onClick={() => {
              //     // !favourites.includes(modalProps)
              //     if (modalAction === "Add to cart") {
              //       addToCart(modalProps.article);
              //     }
              //     if (modalAction === "Remove from cart") {
              //       deleteThisItem(modalProps.article);
              //     }
              //   }}
              //   className="modal-button"
              //   text="Confirm"
              // />,
              <Button
                key={1}
                onClick={() => {
                  if (modalAction === "Add to cart") {
                    addToCart(modalProps.article);
                  }
                  if (modalAction === "Remove from cart") {
                    deleteThisItem(modalProps.article);
                  }
                }}
                className="modal-button"
                text="Confirm"
              />,

              // <Button
              //   key="addToFavourites"
              //   onClick={() => {
              //     addToFavourites(modalProps);
              //   }}
              //   className="modal-button"
              //   text="Cancel"
              // />,
              <Button
                key={2}
                onClick={closeFirstModal}
                className="modal-button"
                text="Cancel"
              />,

              //   {favourites.includes(modalProps)
              //     ? "Deleted from favorites"
              //     : "Added to favorites"}
              // </Button>,
            ]}
          />
          //   <h2>{modalProps.name}</h2>
          //   <img src={modalProps.img} alt={modalProps.name} />
          //   <p>{modalProps.description}</p>
          // </Modal>
        )}
      </>
    </Router>
  );
}

export default App;
