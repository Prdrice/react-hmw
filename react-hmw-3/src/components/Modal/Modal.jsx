import React from "react";
import styles from "./Modal.module.scss";

const Modal = ({
  closeButton,
  actions,
  close,
  text,
}) => {
  return (
    <div className={styles.modalBackground} onClick={close}>
      <div
        onClick={(e) => {
          e.stopPropagation();
        }}
        className={styles.modalContent}
      >
        <header className={styles.modalContentHeader}>
          {closeButton && (
            <div onClick={close} className={styles.modalCloseBtn}>
              &times;
            </div>
          )}
        </header>
        <div>
          <p className={styles.modalContentText}>
            {/* Do you want to add {modalProps.name} in cart? */}
            {text}
          </p>
          <div className={styles.modalButtons}>{actions}</div>
        </div>
      </div>
    </div>
  );
};

export default Modal;

