import styles from "./Header.module.scss";
import cartIcon from "../../components/icons/cart-outline.svg";
import favIcon from "../../components/icons/star_like.svg";
import { NavLink } from "react-router-dom";

const Header = ({ carts, favourites }) => {

  const cartQt = <span>{carts.length}</span>;
  const favouriteQt = <span>{favourites.length}</span>;

  return (
    <header className={styles.root}>
      {/* <div className="d-flex align-center">
        <img width={40} src="/img/logo.jpg" />
        <div>
          <h3>REACT NIKE SHOP</h3>
           <p className="opacity-6">The best shop of sneakers!</p>
        </div>
      </div> */}
      <ul>
        <li>
          <NavLink to="/">Main</NavLink>
        </li>
        <li>
          <NavLink to="/cart">
            Cart
            <img src={cartIcon} alt="Cart" />
          </NavLink>
          {cartQt}
        </li>
        <li>
          <NavLink to="/favourites">
            Favorites
            <img src={favIcon} alt="Cart" width={26} />
            {favouriteQt}
          </NavLink>
        </li>
      </ul>

      {/* <ul>
          <li className="cu-p">
            <a href="#">
              <img src={cartIcon} alt="Cart" />
            </a>
          </li>
          <li>{cartQt}</li>
        </ul>
        <ul>
          <li className="cu-p">
            <a href="#">
              <img src={favIcon} alt="Cart" width={22} />
            </a>
          </li>
          <li>{favouriteQt}</li>
        </ul> */}
    </header>
  );
};

export default Header;
