import React from "react";
import CardItem from "../CardItem";
import styles from "./CardContainer.module.scss";

const CardContainer = ( {
  addToCart,
  cards,
  openFirstModal,
  setModalProps,
  addToFavourites,
  favourites,
})  => {  
  return (
    <div>
      <ul className={styles.list}>
      {/* { name, path, price, article, isFavourite } */}
        {cards.map((item) => (
          <li key={item.article}>
            <CardItem
              favourite={favourites.includes(item.article)}
              addToCart={addToCart}
              item={item}
              openFirstModal={openFirstModal}
              setModalProps={setModalProps}
              addToFavourites={addToFavourites}
              toCart //Строка toCart добавлена в компонент CardItem как пропс, чтобы обозначить, что данный элемент используется в списке карточек, отображаемых на странице корзины. 
            />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default CardContainer;