import { Routes, Route } from "react-router-dom";
import CartPage from "./pages/CartPage/CartPage";
import NotFoundPage from "./pages/NotFoundPage";
import FavouritesPage from "./pages/FavouritesPage/FavouritesPage";
import MainPage from "./pages/MainPage";

const AppRoutes = ({
  favourites,
  addToCart,
  cards,
  carts,
  openFirstModal,
  setModalProps,
  addToFavourites,
  deleteThisItem,
}) => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <MainPage
            favourites={favourites}
            addToCart={addToCart}
            cards={cards}
            carts={carts}
            openFirstModal={openFirstModal}
            setModalProps={setModalProps}
            addToFavourites={addToFavourites}
          />
        }
      />

      <Route
        path="/cart"
        element={
          <CartPage
            favourites={favourites}
            addToCart={addToCart}
            cards={cards}
            carts={carts}
            openFirstModal={openFirstModal}
            setModalProps={setModalProps}
            addToFavourites={addToFavourites}
            deleteThisItem={deleteThisItem}
          />
        }
      />

      <Route
        path="/favourites"
        element={
          <FavouritesPage
            favourites={favourites}
            addToCart={addToCart}
            cards={cards}
            carts={carts}
            openFirstModal={openFirstModal}
            setModalProps={setModalProps}
            addToFavourites={addToFavourites}
            deleteThisItem={deleteThisItem}
          />
        }
      />

      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  );
};

export default AppRoutes;
