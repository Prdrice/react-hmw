import React from "react";
import styles from "./Card.module.scss";
console.log(styles);

function Card(props) {
  const [isAdded, setIsAdded] = React.useState(false);

  const onClickPlusBtn = () => {
    setIsAdded(!isAdded);
  };

  return (
    <div className={styles.card}>
      <div className={styles.favorite} onClick={props.onClickFavoriteBtn}>
        <img src="./svg/heart.svg" alt="Unliked" />
      </div>
      <img width={150} src={props.path} alt="Sneakers" />

      <h5 className={styles.title}> {props.name} </h5>
      <div className="d-flex justify-between ">
        <div className="d-flex flex-column">
          <p>Article: {props.article} </p>
          <p>Color: {props.color} </p>
          <p>Price:</p>
          <b>{props.price}$</b>
        </div>
        <img
          className={styles.button}
          onClick={onClickPlusBtn}
          src={isAdded ? "/svg/btn-checked.svg" : "/svg/btn-plus.svg"}
          alt="Plus"
        />
      </div>
    </div>
  );
}

export default Card;
