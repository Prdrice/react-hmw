import React from "react";
import CardItem from "../CardItem";
import styles from "./CardContainer.module.scss";

// class CardContainer extends PureComponent {
//   render() {
//     const {
//       addToCart,
//       cards,
//       openFirstModal,
//       setModalProps,
//       addToFavourites,
//       favourites,
//     } = this.props;
//     return (
//       <div>
//         <ul className={styles.list}>
//           {cards.map(({ name, path, price, article, isFavourite }) => (
//             <li key={article}>
//               <CardItem
//                 favourite={favourites.includes(article)}
//                 addToCart={addToCart}
//                 article={article}
//                 name={name}
//                 price={price}
//                 path={path}
//                 isFavourite={isFavourite}
//                 openFirstModal={openFirstModal}
//                 setModalProps={setModalProps}
//                 addToFavourites={addToFavourites}
//               />
//             </li>
//           ))}
//         </ul>
//       </div>
//     );
//   }
// }

// export default CardContainer;

const CardContainer = ( {
  addToCart,
  cards,
  openFirstModal,
  setModalProps,
  addToFavourites,
  favourites,
})  => {  
  return (
    <div>
      <ul className={styles.list}>
      {/* { name, path, price, article, isFavourite } */}
        {cards.map((item) => (
          <li key={item.article}>
            <CardItem
              favourite={favourites.includes(item.article)}
              addToCart={addToCart}
              item={item}
              openFirstModal={openFirstModal}
              setModalProps={setModalProps}
              addToFavourites={addToFavourites}
              toCart //Строка toCart добавлена в компонент CardItem как пропс, чтобы обозначить, что данный элемент используется в списке карточек, отображаемых на странице корзины. 
            />
          </li>
        ))}
      </ul>
    </div>
  );
}

export default CardContainer;

// CardContainer.propTypes = {
//   item: PropTypes.string,
//   openFirstModal: PropTypes.func,
//   setModalProps: PropTypes.func,
//   addToFavourites: PropTypes.func,
// };