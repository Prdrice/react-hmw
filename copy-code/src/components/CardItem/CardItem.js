import styles from "./CardItem.module.scss";
import Button from "../Button/Button";
import starIcon from "../../components/icons/star.png";
import favStarIcon from "../../components/icons/favorite.png";
import PropTypes from "prop-types";

// class CardItem extends PureComponent {
//   render() {
//     const {
//       name,
//       path,
//       price,
//       article,
//       favourite,
//       openFirstModal,
//       setModalProps,
//       addToFavourites,
//     } = this.props;
//     return (
//       <div className={styles.card}>
//         <button type="button" className={styles.likeButton}>
//           <img
//             onClick={() => {
//               setModalProps({ article });
//               addToFavourites(article);
//             }}
//             src={favourite ? favStarIcon : starIcon}
//             alt="Favourite"
//           />
//         </button>
//         <span className={styles.title}>{name}</span>
//         <img className={styles.itemAvatar} src={path} alt={name} />
//         <span className={styles.description}>Price:{price}</span>

//         <div className={styles.btnContainer}>
//           <div className="app__buttons">
//             <Button
//               backgroundColor="green"
//               text="Add to cart"
//               className="modalButton"
//               onClick={() => {
//                 setModalProps({ article, name });
//                 openFirstModal();
//               }}
//             />
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// export default CardItem;

// CardItem.propTypes = {
//   name: PropTypes.string,
//   path: PropTypes.string,
//   price: PropTypes.string,
//   article: PropTypes.string,
//   color: PropTypes.string,
//   favourite: PropTypes.bool,
//   openFirstModal: PropTypes.func,
//   setModalProps: PropTypes.func,
//   addToFavourites: PropTypes.func,
// };

const CardItem = (props) => {
  const {
    item,
    favourite,
    openFirstModal,
    setModalProps,
    addToFavourites,
    toCart,
    fromCart,
  } = props;
  const { name, path, price, article } = item;

  // const [isFavourite, setIsFavourite] = useState(favourite);

  // const handleFavouriteClick = () => {
  //   setModalProps({ article });
  //   addToFavourites(article);
  //   setIsFavourite(!isFavourite);
  // };

  // const handleAddToCartClick = () => {
  //   setModalProps({ article, name });
  //   openFirstModal();
  // };

  return (
    <div className={styles.card}>
      <button type="button" className={styles.likeButton}>
        <img
          onClick={() => {
            setModalProps({ article });
            addToFavourites(article);
          }}
          src={favourite ? favStarIcon : starIcon}
          alt="Favourite"
        />
      </button>
      <span className={styles.title}>{name}</span>
      <img className={styles.itemAvatar} src={path} alt={name} />
      <span className={styles.description}>Price:{price}</span>

      <div className={styles.btnContainer}>
        <div className="app__buttons">
          {toCart && (
            <Button
              backgroundColor="green"
              text="Add to cart"
              className="modalButton"
              onClick={() => {
                setModalProps({ article, name });
                openFirstModal("Add to cart");
              }}
            />
          )}
          {fromCart && (
            <Button
              backgroundColor="blue"
              text={"X"}
              className="modalButton"
              onClick={() => {
                setModalProps({ article, name });
                openFirstModal("Remove from cart");
              }}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default CardItem;

CardItem.propTypes = {
  name: PropTypes.string,
  path: PropTypes.string,
  price: PropTypes.string,
  article: PropTypes.string,
  color: PropTypes.string,
  favourite: PropTypes.bool,
  openFirstModal: PropTypes.func,
  setModalProps: PropTypes.func,
  addToFavourites: PropTypes.func,
};

