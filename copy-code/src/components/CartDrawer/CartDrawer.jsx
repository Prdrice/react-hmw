import React from "react";

function CartDrawer(props) {
  return (
    <div className="overlay">
      {/* style={{ display: "none" }} */}
      <div className="drawer">
        <h2 className="d-flex justify-between mb-30">
          Shopping cart{" "}
          <img onClick={props.onClose} className="removeBtn" src="/svg/btn-remove.svg" alt="Close" />
        </h2>
        <div className="items">
          <div className="cartItem d-flex align-center mb-20">
            <img
              className="mr-10"
              width={90}
              src="/img/air-max-270.jpeg"
              alt="Sneakers"
            />

            <div className="mr-20">
              <p className="mb-5">Nike Air Max 270</p>
              <b>140.00 $</b>
            </div>
            <img className="removeBtn" src="/svg/btn-remove.svg" alt="Remove" />
          </div>

          <div className="cartItem d-flex align-center">
            <img
              className="mr-10"
              width={90}
              src="/img/air-max-270-whi.jpeg"
              alt="Sneakers"
            />

            <div className="mr-20">
              <p className="mb-5">Nike Air Max 260</p>
              <b>140.00 $</b>
            </div>
            <img className="removeBtn" src="/svg/btn-remove.svg" alt="Remove" />
          </div>
        </div>

        <div className="cartTotalBlock">
          <ul>
            <li className="">
              <span>Units:</span>
              <div></div>
              <b>2 pcs.</b>
            </li>
            <li className="">
              <span>Subtotal:</span>
              <div></div>
              <b>280.00 $</b>
            </li>
          </ul>
          <button className="greenButton">Confirmed order</button>
        </div>
      </div>
    </div>
  );
}

export default CartDrawer;
