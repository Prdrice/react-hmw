import React from "react";
import styles from "./Modal.module.scss";

const Modal = ({
  closeButton,
  text,
  actions,
  close,
  modalProps,
  addToCart,
  setModalProps,
}) => {
  return (
    <div className={styles.modalBackground} onClick={close}>
      <div
        onClick={(e) => {
          e.stopPropagation();
        }}
        className={styles.modalContent}
      >
        <header className={styles.modalContentHeader}>
          {closeButton && (
            <div onClick={close} className={styles.modalCloseBtn}>
              &times;
            </div>
          )}
        </header>
        <div>
          <p className={styles.modalContentText}>
            {/* Do you want to add {modalProps.name} in cart? */}
            {text}
          </p>
          <div className={styles.modalButtons}>{actions}</div>
        </div>
      </div>
    </div>
  );
};

export default Modal;

// Здесь мы использовали обычную функцию, которая принимает объект props в качестве параметра. Вместо this.props мы используем деструктуризацию и объявляем все необходимые свойства внутри скобок параметров функции.

// Мы также определили две функции-обработчика событий: handleModalClick и handleBackgroundClick. Первая из них используется для предотвращения закрытия модального окна при клике на его содержимое, а вторая - для закрытия окна при клике на фон.
