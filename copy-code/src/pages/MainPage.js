import React from "react";
import CardContainer from "../components/CardContainer/CardContainer";

const MainPage = ({
  favourites,
  addToCart,
  cards,
  carts,
  openFirstModal,
  setModalProps,
  addToFavourites,
}) => {
  return (
    <main>
      <section>
        <h2>Catalog</h2>
        <CardContainer
          favourites={favourites}
          addToCart={addToCart}
          cards={cards}
          carts={carts}
          openFirstModal={openFirstModal}
          setModalProps={setModalProps}
          addToFavourites={addToFavourites}
        />
      </section>
    </main>
  );
};

export default MainPage;
