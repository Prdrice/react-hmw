import React from "react";
import CardItem from "../../components/CardItem";
import styles from "./FavouritesPage.module.scss";

const FavouritesPage = ({
  favourites,
  addToCart,
  cards,
  carts,
  openFirstModal,
  setModalProps,
  addToFavourites,
}) => {
  if (!JSON.parse(localStorage.getItem("favourites"))) {
    return <h2 className="container">Empty</h2>;
  }

  const favourite = JSON.parse(localStorage.getItem("favourites"));
  const keys = Object.values(favourite);
  const render = cards.filter((item) => {
    return keys.includes(item.article.toString());
  });

  return (
    <>
      <h2>Favourites</h2>
      <div className={styles.favourite}>
        {render.map((item) => {
          return (
            <CardItem
              favourites={favourites.includes(item.article)}
              key={item.article}
              addToCart={addToCart}
              item={item}
              openFirstModal={openFirstModal}
              setModalProps={setModalProps}
              addToFavourites={addToFavourites}
              fromCart
            />
          );
        })}
      </div>
    </>
  );
};

export default FavouritesPage;

