import styles from "./CardItem.module.scss";
import Button from "../Button/Button";
import starIcon from "../../components/icons/star.png";
import favStarIcon from "../../components/icons/favorite.png";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { SHOW_MODAL, ADD_REMOVE_FAVORITE } from "../../redux/types";
import AppContext from "../../context/AppContext";
import { useContext } from "react";


const CardItem = ({ item, toCart, fromCart }) => {
  const { name, path, article, price, favourite } = item;

  const dispatch = useDispatch();

  const { tableView } = useContext(AppContext);
  const viewType = tableView ? styles.tableItem : styles.card;


  const addToFavourite = (id) => {
    dispatch({ type: ADD_REMOVE_FAVORITE, payload: id });
  };

  const showModal = (info, data) => {
    dispatch({ type: SHOW_MODAL, payload: { info, data } });
  };


  return (
    <div className={viewType}>
      <button type="button" className={styles.likeButton}>
        <img
          onClick={() => {
            addToFavourite(article);
          }}
          src={favourite ? favStarIcon : starIcon}
          alt="Favourite"
        />
      </button>
      <span className={styles.title}>{name}</span>
      <img className={styles.itemAvatar} src={path} alt={name} />
      <span className={styles.description}>Price: {price}$</span>

      <div className={styles.btnContainer}>
        <div className="app__buttons">
          {toCart && (
            <Button
              backgroundColor="green"
              text="Add to cart"
              className="modalButton"
              onClick={() => {
                showModal("Add to trash", item);
              }}
            />
          )}
          {fromCart && (
            <Button
              backgroundColor="blue"
              text={"Delete"}
              className="modalButton"
              onClick={() => {
                showModal("Remove from trash", item);
              }}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default CardItem;

CardItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    path: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    favourite: PropTypes.bool,
  }),
  toCart: PropTypes.bool,
  fromCart: PropTypes.bool,
};




