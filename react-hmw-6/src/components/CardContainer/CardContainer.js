import React from "react";
import CardItem from "../CardItem";
import styles from "./CardContainer.module.scss";
import { useSelector, shallowEqual } from "react-redux";
import AppContext from "../../context/AppContext";
import Button from "../Button/Button";


const CardContainer = () => {
  const cards = useSelector((state) => state.items.data, shallowEqual);
  const { tableView, changeTypeView } = React.useContext(AppContext);
  const viewType = tableView ? styles.table : styles.list;


  return (
    <div>
      <Button
        type="submit"
        backgroundColor="green"
        onClick={() => changeTypeView(tableView)}
        text={tableView ? "Cards View" : "Table View"}
      ></Button>
      <ul className={viewType}
      // className={styles.list}
      >
        {cards.map((item) => (
          <li key={item.path}>
            <CardItem
              key={item.article}
              item={item}
              toCart
            />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default CardContainer;
