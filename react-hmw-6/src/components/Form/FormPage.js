import React from "react";
import styles from "./Form.module.scss";
import Button from "../Button/Button";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { setTrash, getTrash } from "../../redux/middleware";
import { REMOVE_FROM_TRASH, MAKE_ORDER } from "../../redux/types";
import { useDispatch } from "react-redux";
import { useSelector, shallowEqual } from "react-redux";

const FormPage = () => {
  const initialValues = {
    name: "",
    surname: "",
    age: "",
    adress: "",
    phone: "",
  };
  // Создание схемы проверки с помощью yup
  const validationForm = yup.object().shape({
    name: yup
      .string()
      .min(3, "Your name cant be less than 3 symbols")
      .max(10, "Your name cant be more than 10 symbols")
      .required("Name field is required"),

    surname: yup
      .string()
      .min(2, "Your surname cant be less than 2 symbols")
      .max(20, "Your surname cant be more than 20 symbols")
      .required("Surname field is required"),

    age: yup
      .number()
      .min(18, "You must be 18 or older")
      .max(100, "You must be 99 or older")
      .positive("Age must be a positive number")
      .integer("Age must be an integer")
      .required("Age field is required"),

    adress: yup
      .string()
      .min(6, "Your adress cant be less than 6 symbols")
      .required("Adress field is required"),

    phone: yup
      .number()
      .min(8, "Your phone cant be less than 8 symbols")
      .required("Phone field is required"),
  });

  const dispatch = useDispatch();
  const items = useSelector((state) => state.items.data, shallowEqual);

  const removeFromTrash = (data) => {
    dispatch({ type: REMOVE_FROM_TRASH, data });
  };

  const handleMakeOrder = (data) => {
    dispatch({ type: MAKE_ORDER, data });
  };

  const handleSubmit = (values, { resetForm }) => {
    const data = getTrash();
    const order = items.filter((item) => item.inTrashAmount);
    handleMakeOrder(order);
    Object.keys(data).forEach((el) => {
      removeFromTrash(el);
    });
    console.log(order);
    setTrash({});
    console.log(values);
    resetForm();
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validationForm}
    >
      {() => {
        return (
          <Form>
            <Field type="text" name="name" placeholder="Name" />
            <ErrorMessage name="name">
              {(msg) => <span className={styles.error}>{msg}</span>}
            </ErrorMessage>

            <Field type="text" name="surname" placeholder="Surname" />
            <ErrorMessage name="surname">
              {(msg) => <span className={styles.error}>{msg}</span>}
            </ErrorMessage>

            <Field type="text" name="age" placeholder="Age" />
            <ErrorMessage name="age">
              {(msg) => <span className={styles.error}>{msg}</span>}
            </ErrorMessage>

            <Field type="text" name="adress" placeholder="Adress" />
            <ErrorMessage name="adress">
              {(msg) => <span className={styles.error}>{msg}</span>}
            </ErrorMessage>

            <Field type="phone" name="phone" placeholder="Phone" />
            <ErrorMessage name="phone">
              {(msg) => <span className={styles.error}>{msg}</span>}
            </ErrorMessage>

            <Button
              type="submit"
              text="Checkout"
            ></Button>
          </Form>
        );
      }}
    </Formik>
  );
};

export default FormPage;