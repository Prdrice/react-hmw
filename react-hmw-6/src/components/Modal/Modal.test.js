import Modal from "./Modal";
import {
  render,
  fireEvent,
  screen,
} from "@testing-library/react";
import "@testing-library/jest-dom";

const idModal = "modalJest";
const idButton = "closeButtonJest";

//Создается замена (mock) для библиотеки "react-redux", в частности для хука useSelector, чтобы он возвращал предопределенное значение при вызове.
jest.mock("react-redux", () => ({
  useSelector: () => {
    return { info: "testAction", data: { name: "itemName" } };
  },
}));

describe("Modal.js", () => {
  //тестирует отображение модального окна без дополнительных параметров
  test("Modal - Render", () => {
    render(<Modal className="modalBackground" />);
  });

  //выполняет снимок (snapshot) компонента <Modal> и сравнивает его с предыдущим
  test("Modal snapshot test", () => {
    const { container } = render(<Modal />);
    expect(container.innerHTML).toMatchSnapshot();
  });

  test("Modal snapshot test found modalJest", () => {
    render(<Modal />);
    // Проверка наличия элемента с атрибутом data-testid="modalJest"
    const modalElement = screen.queryByTestId("modalJest");
    expect(modalElement).toBeInTheDocument();
  });

  //тестирует отображение модального окна с переданным массивом действий (actions)
  test("Modal action", () => {
    const action = ["Action"];
    render(<Modal actions={action} />);
  });

  //проверяет, что модальное окно закрывается при клике, и что функция closeModal вызывается в результате этого действия.
  test("should close Modal window", () => {
    // Создается мок (mock) функции closeModal с помощью jest.fn(), чтобы отслеживать вызовы этой функции.
    const closeModal = jest.fn();
    //С помощью render из библиотеки "@testing-library/react" отображается компонент <Modal>, передавая ему созданную мок-функцию closeModal
    const { getByTestId } = render(<Modal closeModal={closeModal} />);
    //С помощью fireEvent.click из той же библиотеки симулируется клик на элементе модального окна, найденном с помощью screen...
    fireEvent.click(screen.getByTestId(idModal));
    //проверяется, была ли вызвана функция closeModal в результате клика.
    expect(closeModal).toHaveBeenCalled();
  });

  //проверяет, что модальное окно закрывается при клике на кнопку закрытия (close button), и что функция closeModal вызывается или нет
  test("should testing close Button", () => {
    const closeModal = jest.fn();
    const { getByTestId } = render(
      <Modal closeModal={closeModal} closeButton />
    );
    fireEvent.click(screen.getByTestId(idButton));
    expect(closeModal).toHaveBeenCalled();
  });

  //проверяет, что событие click на элементе модального окна предотвращает всплытие
  test("should click on modal and stop showing this modal window", () => {
    const { getByTestId } = render(<Modal />);
    const modalBody = getByTestId("modalJest");
    expect(modalBody).toBeInTheDocument();
    fireEvent.click(modalBody);
  });

});
