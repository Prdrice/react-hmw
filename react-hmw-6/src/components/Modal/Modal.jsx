import React from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";


const Modal = ({ closeButton, actions, closeModal }) => {
  const { info: action, data: item } = useSelector((state) => state.modal);

  return (
    <div className={styles.modalBackground} onClick={closeModal}
    data-testid="modalJest">
      <div
        onClick={(e) => {
          e.stopPropagation();
        }}
        className={styles.modalContent}
      >
        <header className={styles.modalContentHeader}>
          {closeButton && (
            <div onClick={closeModal} data-testid="closeButtonJest" className={styles.modalCloseBtn}>
              &times;
            </div>
          )}
        </header>
        <div>
          <p className={styles.modalContentText}>
            {`${action} ${item.name}?`}</p>
          <div className={styles.modalButtons}>{actions}</div>
        </div>
      </div>
    </div>
  );
};

export default Modal;

Modal.propTypes = {
  closeModal: PropTypes.func,
  closeButton: PropTypes.bool,
  actions: PropTypes.array,
};

