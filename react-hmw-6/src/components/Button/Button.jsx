import React from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

function Button({ backgroundColor, text, onClick, type }) {
  return (
    <button
      className={styles.modalButton}
      style={{ backgroundColor }}
      onClick={onClick}
      type={type}
    >
      {text}
    </button>
  );
}

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  text: "",
  onClick: () => {},
};

export default Button;
