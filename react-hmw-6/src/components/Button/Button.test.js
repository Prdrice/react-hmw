import { render, screen, fireEvent } from "@testing-library/react";
import Button from "./Button";

const buttonClick = jest.fn();

describe("Button snapshot test", () => {
  test("Button renders", () => {
    const { asFragment } = render(
      <Button text="test text" type="Button" backgroundColor="green" />
    );
    expect(asFragment()).toMatchSnapshot();
  });
});

describe("Button click works", () => {
  test("Should buttonClick work when button clicked ", () => {
    render(
      <Button
        onClick={buttonClick}
        text="test text"
        type="Button"
        backgroundColor="green"
      />
    );

    fireEvent.click(screen.getByText("test text")); 

    expect(buttonClick).toHaveBeenCalled();
  });
});
