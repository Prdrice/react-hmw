import React from "react";
import PropTypes from "prop-types";
import AppContext from "./AppContext";

// создаем обертку над контекстом приложения AppContext
const AppContextProvider = ({ value, children }) => {
  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
};
// Пропсы value и children определены с помощью propTypes из пакета prop-types. 
// Это позволяет задать тип и обязательность этих пропсов.

AppContextProvider.propTypes = {
//Тип value задан как PropTypes.any, что означает, что пропс может иметь любой тип значения.
  value: PropTypes.any.isRequired,
  children: PropTypes.oneOfType([
//Пропс children задан с помощью PropTypes.oneOfType, что позволяет принимать либо один React-элемент (PropTypes.node), либо массив из React-элементов (PropTypes.arrayOf(PropTypes.node)).
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
};

export default AppContextProvider;
