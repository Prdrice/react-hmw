import { SUCCESS } from "./types";

export const getTrash = () => {
  const carts = localStorage.getItem("Cart");
  return carts ? JSON.parse(carts) : {};
};
//Получает данные корзины из локального хранилища.Если данные присутствуют, парсит их из JSON формата в объект. Если данных нет, возвращает пустой объект. 

export const setTrash = (data) => {
  localStorage.setItem("Cart", JSON.stringify(data));
};// Устанавливает данные корзины в локальное хранилище. 

export const getFavorites = () => {
  const favouritesLS = localStorage.getItem("Favourite");
  return favouritesLS ? JSON.parse(favouritesLS) : [];
};//Получает список избранных элементов

export const setFavorites = (data) => {
  localStorage.setItem("Favourite", JSON.stringify(data));
};

export const loadItems = () => async (dispatch) => {
 //асинхронная загрузка элементов
  const favourites = getFavorites();
  const trash = getTrash();

  try {
    const data = await fetch(`./items.json`).then((res) => res.json());
    //Выполняет асинхронный запрос с использованием fetch, чтобы получить данные из файла "items.json".

    const newItems = data.map((item) => {
      item.favourite = favourites.includes(item.article);
      item.inTrashAmount = trash[item.article] || null;
      return item;
    });
    dispatch({ type: SUCCESS, payload: newItems });
  } catch (error) {
    console.log(error);
  }
};

