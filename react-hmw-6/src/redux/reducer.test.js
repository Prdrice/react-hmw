import reducer from "./reducer";
import {
  SUCCESS,
  SHOW_MODAL,
  ADD_REMOVE_FAVORITE,
  CLOSE_MODAL,
  ADD_TO_TRASH,
  REMOVE_FROM_TRASH,
} from "./types";

describe("reducer", () => {
  const initialState = {
    items: {
      data: [],
      isLoading: true,
    },
    modal: {
      info: "",
      data: null,
      isOpen: false,
    },
    order: {},
  };

  test("return initial state", () => {
    expect(reducer(initialState, { type: "" })).toEqual(initialState);
  });

  test("return open modal", () => {
    const payload = { info: "Modal info", data: {} };
    const expectedState = {
      ...initialState,
      modal: {
        ...initialState.modal,
        info: payload.info,
        data: payload.data,
        isOpen: true,
      },
    };
    const action = { type: SHOW_MODAL, payload };
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  test("return close modal", () => {
    const openModal = {
      ...initialState,
      modal: {
        ...initialState.modal,
        isOpen: true,
      },
    };
    const expectedState = {
      ...initialState,
      modal: {
        ...initialState.modal,
        isOpen: false,
      },
    };
    const action = { type: CLOSE_MODAL };
    expect(reducer(openModal, action)).toEqual(expectedState);
  });

  test("return add favorite", () => {
    const initialState = {
      items: {
        data: [
          { article: "article1", favourite: false },
          { article: "article2", favourite: true },
        ],
        isLoading: true,
      },
      modal: {
        info: "",
        data: null,
        isOpen: false,
      },
      order: {},
    };

    const expectedState = {
      ...initialState,
      items: {
        data: [
          { article: "article1", favourite: true },
          { article: "article2", favourite: true },
        ],
        isLoading: true,
      },
    };

    const action = { type: ADD_REMOVE_FAVORITE, payload: "article1" };
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  test("return add to trash", () => {
    const openModal = {
      ...initialState,
      items: {
        data: [
          { article: "article1", inTrashAmount: 0 },
          { article: "article2", inTrashAmount: 2 },
        ],
        isLoading: true,
      },
    };
    const expectedState = {
      ...initialState,
      items: {
        data: [
          { article: "article1", inTrashAmount: 1 },
          { article: "article2", inTrashAmount: 2 },
        ],
        isLoading: true,
      },
    };
    const action = { type: ADD_TO_TRASH, payload: "article1" };
    expect(reducer(openModal, action)).toEqual(expectedState);
  });

  test("return remove from trash", () => {
    const openModal = {
      ...initialState,
      items: {
        data: [
          { article: "article1", inTrashAmount: 2 },
          { article: "article2", inTrashAmount: 1 },
        ],
        isLoading: true,
      },
    };
    const expectedState = {
      ...initialState,
      items: {
        data: [
          { article: "article1", inTrashAmount: 1 },
          { article: "article2", inTrashAmount: 1 },
        ],
        isLoading: true,
      },
    };
    const action = { type: REMOVE_FROM_TRASH, payload: "article1" };
    expect(reducer(openModal, action)).toEqual(expectedState);
  });

  test("return success", () => {
    const payload = [1, 2, 3];
    const expectedState = {
      ...initialState,
      items: {
        data: payload,
        isLoading: false,
      },
    };
    const action = { type: SUCCESS, payload };
    expect(reducer(initialState, action)).toEqual(expectedState);
  });
});
